var gulp = require('gulp');
var concat = require('gulp-concat');
var uglifyjs = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var browserSync = require('browser-sync').create();

gulp.task('default',function(){
 //for css
  gulp.src([
      './source/test1.css',
      './source/test2.css'
  ])
  .pipe(concat("bundle.min.css"))
  .pipe(uglifycss())
  .pipe(gulp.dest('./public/assets/css'));

 //for js
  gulp.src([
      './source/js/test2.js',
      './source/js/test1.js'
  ])
  .pipe(concat("bundle.min.js"))
  .pipe(uglifyjs())
  .pipe(gulp.dest('./public/assets/js'));

});

// Rerun the task when a file changes
gulp.task('watch', function() {
    browserSync.init({
        server: {
            baseDir: "./public/"
        }
    });

    gulp.watch('./source/**/*', ['default']);
    gulp.watch("public/**/*").on('change', browserSync.reload);
});
